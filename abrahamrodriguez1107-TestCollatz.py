#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)
    
    def test_read_2(self):
        s = "0 1000000\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 0)
        self.assertEqual(j, 1000000)
        
    def test_read_3(self):
        s = "160357 514383\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 160357)
        self.assertEqual(j, 514383)
        
    def test_read_4(self):
        s = "46 47\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 46)
        self.assertEqual(j, 47)
        
    def test_read_5(self):
        s = "200 200000\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 200)
        self.assertEqual(j, 200000)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)
    
    def test_eval_5(self):
        v = collatz_eval(47, 999999)
        self.assertEqual(v, 525)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")
        
    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 47, 999999, 525)
        self.assertEqual(w.getvalue(), "47 999999 525\n")
        
    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 46, 47, 105)
        self.assertEqual(w.getvalue(), "46 47 105\n")
        
    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 201, 210, 89)
        self.assertEqual(w.getvalue(), "201 210 89\n")
        
    def test_print_5(self):
        w = StringIO()
        collatz_print(w, 105, 525, 144)
        self.assertEqual(w.getvalue(), "105 525 144\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO("500000 999999\n69 420\n45 45000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "500000 999999 525\n69 420 144\n45 45000 324\n")
            
    def test_solve_3(self):
        r = StringIO("40 400\n154 190455\n7 777\n10 10000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "40 400 144\n154 190455 383\n7 777 171\n10 10000 262\n")
            
    def test_solve_4(self):
        r = StringIO("39 4959\n72 150\n279 428\n431 477\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "39 4959 238\n72 150 122\n279 428 144\n431 477 129\n")
            
    def test_solve_5(self):
        r = StringIO("1081 1111\n1112 1379\n1395 1437\n1461 1478\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1081 1111 138\n1112 1379 182\n1395 1437 172\n1461 1478 172\n")

    
# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
